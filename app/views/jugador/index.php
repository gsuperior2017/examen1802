<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Vista de jugadores</h1>

      <p class="lead">Detalles de los jugadores de la seleccion</p>
      <p><a href="/jugador/create">Nueva entrada</a></p>
      <p><?php echo "$_SESSION[gestion]" ?></p>
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Id. Jugador</th>
            <th>Nombre</th>
            <th>Id. Puesto</th>
            <th>Nacimiento</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <?php foreach ($jugadores as $key => $jugador): ?>
          <tr>
            <td><?php echo "$jugador->id" ?></td>
            <td><?php echo "$jugador->nombre" ?></td>
            <td>
              <?php
                foreach ($puestos  as $key => $puesto) {
                  $id = $jugador->id_puesto;
                  if (($key + 1) == $id) {
                    echo "$puesto->nombre";
                  }
                }

              ?>

              </td>
            <!--$jugador->findPuesto($jugador->id_puesto)-->
            <td>
              <?php
                $date = new DateTime($jugador->nacimiento);
                echo $date->format('d-m-Y')
              ?>
            </td>
            <td>
              <a href="/jugador/titular/<?php echo $jugador->id ?>">Añadir titular</a>
            </td>

          </tr>
        <?php endforeach ?>
        <tr>
          <td colspan="5"> <hr></td>
        </tr>
      </table>


    </div>

  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>

</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
