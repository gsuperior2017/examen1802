<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Titulares para el proximo partido</h1>
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Id. Jugador</th>
            <th>Nombre</th>
            <th>Id. Puesto</th>
            <th>Nacimiento</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <?php foreach ($_SESSION['titulares'] as $key => $titular): ?>
          <tr>
            <td><?php echo "$titular->id" ?></td>
            <td><?php echo "$titular->nombre" ?></td>
            <td>
              <?php
                foreach ($puestos  as $key => $puesto) {
                  $id = $titular->id_puesto;
                  if (($key + 1) == $id) {
                    echo "$puesto->nombre";
                  }
                }

              ?>

              </td>
            <!--$jugador->findPuesto($jugador->id_puesto)-->
            <td>
              <?php
                $date = new DateTime($titular->nacimiento);
                echo $date->format('d-m-Y')
              ?>
            </td>
            <td>
              <a href="/jugador/quitar/<?php echo $titular->id ?>">Quitar titular</a>
            </td>

          </tr>
        <?php endforeach ?>
        <tr>
          <td colspan="5"><a href="/jugador">Volver -</a></td>
        </tr>
      </table>


    </div>

  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>

</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
