<!doctype html>
<html lang="es">
  <head>
    <?php require "../app/views/parts/head.php" ?>
  </head>
    <body>
      <?php require "../app/views/parts/header.php" ?>
      <main role="main" class="container">
        <br><br><br>
        <div class="starter-template">

        <h1>Nueva alta de jugador</h1>

         <form action="/jugador/insert" method="post">

          <div class="form-group">
            <label >Nombre: </label>
            <input type="text" class="form-control" name="nombre">
          </div>
          <div class="form-group">
            <label >Puesto:</label>
            <select name="id_puesto">
              <?php foreach ($puestos as $key => $puesto): ?>
                <option value="<?php echo $key ?>"><?php echo "$puesto->nombre" ?></option>
              <?php endforeach ?>
            </select>
          </div>
          <div class="form-group">
            <label >Fecha de nacimiento:</label>
            <input type="text" class="form-control" name="fecha">
          </div>
          <div form-group>
            <label><?php echo "$_SESSION[error]" ?></label>
          </div>
          <button type="submit" class="btn btn-default">Submit</button>

        </form>

      </div>

    </main><!-- /.container -->
    <?php require "../app/views/parts/footer.php" ?>
    </body>
    <?php require "../app/views/parts/scripts.php" ?>
</html>
