<?php
namespace App\Models;

use PDO;
use Core\Model;

require_once '../core/Model.php';
/**
*
*/
class User extends Model
{

    function __construct()
    {

    }

    public function jugadores()
    {
        $db = User::db();
        $jugadores = new User();
        $stmt = $db->prepare('SELECT * FROM jugadores');
        $jugadores = $stmt->execute();
        $jugadores = $stmt->fetchAll(PDO::FETCH_CLASS, User::class);

        return $jugadores;
    }

    public function puestos()
    {
        $db = User::db();
        $stmt = $db->prepare('SELECT * from puestos');
        $stmt->execute();

        $puestos = $stmt->fetchAll(PDO::FETCH_CLASS, User::class);

        return $puestos;
    }

    public function store (){
        $db = User::db();
        $stmt = $db->prepare('INSERT INTO jugadores(nombre, id_puesto, nacimiento)
            VALUES(:nombre, :id_puesto, :nacimiento)');
        $stmt->bindValue(':nombre', $this->nombre);
        $stmt->bindValue(':id_puesto', $this->id_puesto);
        $stmt->bindValue(':nacimiento', $this->nacimiento);

        return $stmt->execute();
    }

    public function findJugador($id)
    {
        $db = User::db();
        $stmt = $db->prepare('SELECT * FROM jugadores WHERE id = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();

        $jugador = $stmt->fetchAll(PDO::FETCH_CLASS, User::class);
        return $jugador[0];
    }

    public function findPuesto ($id){
        $db = User::db();
        $stmt = $db->prepare('SELECT * FROM puestos WHERE id = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $puesto = $stmt->fetchAll(PDO::FETCH_CLASS, User::class);

        return $puesto->nombre;
    }


}
