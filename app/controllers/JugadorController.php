<?php
namespace App\Controllers;

require_once '../app/models/User.php';

use \App\Models\User;
/**
*
*/
use PDO;

class JugadorController
{

    function __construct()
    {
        if (!isset($_SESSION['error'])) {
            $_SESSION['error'] = " ";
        } else if (!isset($_SESSION['titulares'])) {
            $_SESSION['titulares'] = [];
        } else if (!isset($_SESSION['gestion'])) {
            $_SESSION['gestion'] = "";
        }
    }

    public function index()
    {
        $jugadores = User::jugadores();
        $puestos = User::puestos();
        //$jugadores = User::paginate(5);

        //$rowCount  = User::rowCount();
        if(isset($rowCount))
        {
            $pages = ceil($rowCount/4); //funcion ceil
        } else {
            $pages = 0;
        }
        require "../app/views/jugador/index.php";
    }

    public function findPuesto()
     {
        $puesto = User::findPuesto($this->id_puesto);

        return $puesto;
    }

    public function create()
    {
        $puestos = User::puestos();
        require "../app/views/jugador/create.php";
    }

    public function insert()
    {
        $id = (int)($_REQUEST['id_puesto']) + 1;
        if(isset($_REQUEST['nombre']) && !empty($_REQUEST['nombre']) && isset($_REQUEST['fecha']) && !empty($_REQUEST['fecha']) && isset($_REQUEST['id_puesto']) && !empty($_REQUEST['id_puesto']))
        {
            $jugador = new User();
            $jugador->id_puesto = $id;
            $jugador->nombre = $_REQUEST['nombre'];
            $jugador->nacimiento = $_REQUEST['fecha'];
            $jugador->store();
            $_SESSION['error'] = "";
            header("Location:/jugador");

        } else {
            $_SESSION['error'] = "Error al insertar los datos. Intentelo de nuevo";
            header("Location:/jugador/create");
        }
    }

    public function titular($arguments)
    {
        $idJugador = (int)($arguments[0]);
        $jugador = User::findJugador($idJugador);

        if (!isset($_SESSION['titulares'][$jugador->id])) {
            $_SESSION['titulares'][$jugador->id] = $jugador;
            $_SESSION['gestion'] = "";
            header("Location:/jugador/titulares");
        } else {
            $_SESSION['gestion'] = "Este jugador ya es titular; intentelo mas tarde";
            header("Location:/jugador");
        }
    }

    public function titulares()
    {
        $puestos = User::puestos();
        require "../app/views/jugador/titulares.php";
    }

    public function quitar($arguments){
        $idJugador = (int)($arguments[0]);
        unset($_SESSION['titulares'][$idJugador]);
        $_SESSION['gestion'] = "Jugador quitado de titulares";
        // echo "<pre>";
        // var_dump($_SESSION['titulares']);
        // exit;
        header("Location:/jugador/titulares");
    }
}
